//    TapTempo, a command line tap tempo.
//    Copyright (C) 2017 Francois Mazen
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "taptempo.h"

#include <random>

class TapTempoGame : public TapTempo
{
public:
    TapTempoGame(size_t sampleSize, size_t resetTimeInSecond, size_t precision);
    
protected:
    virtual void displayBPM(double bpm) override;

private:
    double computeNewSecretBPM();
    bool areBPMEquals(double firstBPM, double secondBPM) const;

    double secretBPM;
    std::random_device randomDevice;
};
