TapTempo Changelog
==================

## 1.4.6 - 2025-01-17

- Add manual page
- Update URLs in README since Tuxfamily is closing

## 1.4.5 - 2020-07-02

- Fix ctrl-D (EOF) issue.
- Update build requirements in README.md

## 1.4.4 - 2019-01-21

- Fix FindCatch.cmake with Catch2 package.

## 1.4.3 - 2018-09-15

- Remove mingw-bundledlls.py from the source, and update Windows build instructions.

## 1.4.2 - 2018-08-15

- Fix memory leak in game mode.
- Fix potential crash in main loop.

## 1.4.1 - 2018-07-05

- Change "check" target to "test" target, to match debian packaging system.
- Fix Options class when calling multiple times getopt_long

## 1.4.0 - 2018-06-29

- Add gaming mode with --game switch.
- Add unit tests using Catch framework.
- Move debian package code to a separate repository.

## 1.3.0 - 2018-03-26

- When input arguments are negative, the value is set to default value instead of max value.
- When two hits are at same timestamp, infinity is displayed.
- Replace STRINGIFY by CMake variables in taptempo_config.h.in

## 1.2.1 - 2018-03-18

- Force C++11 in CMake file to fix build error.

## 1.2.0 - 2018-01-29

- Change display decimal precision to 0 digit.
- Add command line arguments to change reset time, sample count and precision.
- Add help message and version.
- Update man page.

## 1.1.2 - 2017-10-24

- Add debian package files.

## 1.1.1 - 2017-10-19

- Script to build for Windows in MSYS2.

## 1.1.0 - 2017-10-17

- Reset bpm computation when no entry in 5 seconds.
- Improve CMakeLists.txt with install and gettext/Intl.

## 1.0.0 - 2017-10-15

- Initial release, `GPL-3.0+`.
