TapTempo build on windows platform
==================================

* download the file catch.hpp from Catch2 project at https://github.com/catchorg/Catch2/releases and copy it in msys folder.
* download the file mingw-bundledlls from https://github.com/mpreisler/mingw-bundledlls and copy it in msys folder.
* check that you can execute mingw-bundledlls, otherwise run `chmod +x mingw-bundledlls`
* download and install MSYS2: https://www.msys2.org/
* launch MSYS2 console, navigate to the msys folder and launch msys2_build.sh

