#!/bin/bash
set -e

cd ..
echo ==== BUILD DOCKER ====
docker build -t taptempo-msys2 -f msys/Dockerfile .
cd msys

echo ==== RUN DOCKER ====
docker run -v ${PWD}/artifacts:/artifacts taptempo-msys2
