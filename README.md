Tap Tempo
=========

A command line tap tempo. Hit enter key with style and you'll get the corresponding number of beats per minute (bpm).

This tool is very useful to quickly find the tempo of a song without launching a big digital workstation like Ardour or LMMS.

Website: https://mzf.fr/taptempo

Sources: https://code.mzf.fr/mzf/taptempo.git

Debian Packaging: https://salsa.debian.org/mzf/taptempo.git

## Demo

Just type `taptempo` in a terminal and hit the enter key while you're hearing a song to print the tempo:

```
> taptempo
Hit enter key for each beat (q to quit).

[Hit enter key one more time to start bpm computation...]
Tempo: 143 bpm	
Tempo: 145 bpm	
Tempo: 143 bpm	
Tempo: 143 bpm	
Tempo: 144 bpm	
Tempo: 145 bpm	
Tempo: 146 bpm	q
Bye Bye!

```

## Download

Check the [download server](https://mzf.fr/taptempo/download/) for latest release.

### Linux

Many Linux distributions like Debian, Ubuntu or Arch Linux have packaged TapTempo. Please check [Repology](https://repology.org/project/taptempo/versions).

### Windows

Check the [download server](https://mzf.fr/taptempo/download/) to get the latest release, usually named: `taptempo-x.y.z-win-x64.7z`

### Sources
Signed tarballs are available at the [download server](https://mzf.fr/taptempo/download/). For the latest development version, check the [official git repository](https://code.mzf.fr/mzf/taptempo.git).

## License

GNU General Public License v3.0 (`GPL-3.0+`, see `LICENSE.txt`).

## Build instructions

### Requirements:
- [CMake](https://cmake.org/)
- [Gettext & Intl](https://www.gnu.org/software/gettext/)
- [Catch2](https://github.com/catchorg/Catch2)

### Linux

```bash
mkdir _build
cd _build
cmake ..
make
make DESTDIR="/path/to/install" install
./path/to/install/bin/taptempo
```

To generate debian package, check out the [packaging repo](https://salsa.debian.org/mzf/taptempo.git) and type:

```bash
debuild -b -us -uc
```

### Windows

Install [MSYS2](http://www.msys2.org/) and run `msys/msys2_build.sh` script inside MSYS2 shell.

See `msys/Readme.txt` file for detailed instructions.

## Authors

Main developer:

- François Mazen <francois@mzf.fr>

Other contributors:

- David Demelier <markand@malikania.fr>

## Misc

TapTempo is written in C++, but lots of porting in other languages have been developed via the [LinuxFr website](https://linuxfr.org/tags/taptempo/public).


